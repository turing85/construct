package com.turing.construct.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for classes to generate builders for.
 *
 * <p>This is the annotation to start builder generation. All classes annotated with thin annotation
 * are subject to procession.
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface Buildable {}
