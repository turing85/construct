package com.turing.constructtest;

import com.turing.construct.annotation.Buildable;
import com.turing.construct.impl.annotations.primitives.BooleanBuildProperty;
import com.turing.construct.impl.annotations.primitives.ByteBuildProperty;
import com.turing.construct.impl.annotations.primitives.CharBuildProperty;
import com.turing.construct.impl.annotations.primitives.DoubleBuildProperty;
import com.turing.construct.impl.annotations.primitives.FloatBuildProperty;
import com.turing.construct.impl.annotations.primitives.IntBuildProperty;
import com.turing.construct.impl.annotations.primitives.LongBuildProperty;
import com.turing.construct.impl.annotations.primitives.ShortBuildProperty;
import com.turing.construct.impl.annotations.wrappers.BooleanWrapperBuildProperty;
import com.turing.construct.impl.annotations.wrappers.ByteWrapperBuildProperty;
import com.turing.construct.impl.annotations.wrappers.CharacterWrapperBuildProperty;
import com.turing.construct.impl.annotations.wrappers.DoubleWrapperBuildProperty;
import com.turing.construct.impl.annotations.wrappers.FloatWrapperBuildProperty;
import com.turing.construct.impl.annotations.wrappers.IntegerWrapperBuildProperty;
import com.turing.construct.impl.annotations.wrappers.LongWrapperBuildProperty;
import com.turing.construct.impl.annotations.wrappers.ShortWrapperBuildProperty;
import javax.validation.constraints.NotNull;

@Buildable
public class Person {

  @IntBuildProperty(lowerBound = 0, defaultValue = 2000)
  Integer income;

  @BooleanBuildProperty boolean hasSomeProperty;

  @BooleanWrapperBuildProperty Boolean someOtherMoreComplexProperty;

  @NotNull
  @IntegerWrapperBuildProperty(lowerBound = 100, upperBound = 1000, hasDefaultValue = false)
  Integer userId;

  @ShortBuildProperty short someShort;

  @ByteBuildProperty byte password;

  @DoubleBuildProperty double moneyz;

  @FloatBuildProperty float allDemMonez;

  @LongBuildProperty long longlong;

  @CharBuildProperty char charchar;

  @DoubleWrapperBuildProperty Double someDouble;

  @NotNull @ByteWrapperBuildProperty Byte bytes;

  @ShortWrapperBuildProperty Short shorty;

  @LongWrapperBuildProperty Long uuid;

  @FloatWrapperBuildProperty Float floater;

  @CharacterWrapperBuildProperty Character characteristic;

  @IntBuildProperty(lowerBound = 0, upperBound = 120, defaultValue = 18)
  private Object age;

  // @DoubleBuildProperty
  // Person child;

  public Person(int age, short someShort) {}
}
