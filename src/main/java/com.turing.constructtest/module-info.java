module com.turing.constructtest {
  requires com.turing.construct.annotations;
  requires com.turing.construct.impl;
  requires java.validation;
}
