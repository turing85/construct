module com.turing.construct {
  requires com.turing.construct.annotations;
  requires java.compiler;
  requires java.validation;

  exports com.turing.construct;
  exports com.turing.construct.processors;
  exports com.turing.construct.model;

  uses com.turing.construct.processors.FieldGenerator;
  uses com.turing.construct.processors.MethodGenerator;

  provides javax.annotation.processing.Processor with
      com.turing.construct.BuilderGenerator;
}
