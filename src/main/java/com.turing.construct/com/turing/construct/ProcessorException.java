package com.turing.construct;

/** Exception thrown when something goes wrong during annotation processing. */
public class ProcessorException extends Exception {}
