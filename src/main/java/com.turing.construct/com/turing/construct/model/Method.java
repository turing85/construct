package com.turing.construct.model;

import com.turing.construct.Exceptions;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * Representation of a class-method of the generated builder.
 *
 * <p>A {@code Method} is always created for a specific field, i.e. to generate a method {@code
 * with<fieldName>(<fieldType> <fieldName>)}.
 *
 * <p>To allow a fluent interface, the return-type of this method is the type of the surrounding
 * class, thus the method must know the class it is a method of.
 */
public class Method {

  /** The surrounding {@link Clazz}. */
  @NotNull private final Clazz inClass;

  /** The {@link Field}, this method is generated for. */
  @NotNull private final Field forField;

  /**
   * Constructor.
   *
   * @param inClazz the surrounding {@code Clazz}.
   * @param forField the {@code Field}, this method is generated for.
   * @throws IllegalArgumentException if argument {@code inClazz} or {@code forField} is {@code
   *     null}.
   */
  public Method(@NotNull final Clazz inClazz, @NotNull final Field forField)
      throws IllegalArgumentException {
    this.inClass = Objects.requireNonNullElseGet(inClazz, Exceptions::illegalArgument);
    this.forField = Objects.requireNonNullElseGet(forField, Exceptions::illegalArgument);
  }

  @NotNull
  public Field forField() {
    return this.forField;
  }

  @NotNull
  public Clazz inClass() {
    return this.inClass;
  }

  @NotNull
  public String name() {
    final String firstLetterUpperCase = this.forField().name().substring(0, 1).toUpperCase();
    final String restAsIs = this.forField.name().substring(1);
    return "with" + firstLetterUpperCase + restAsIs;
  }
}
