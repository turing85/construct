package com.turing.construct.model;

import com.turing.construct.Exceptions;
import java.util.Objects;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/**
 * A field with bounds, i.e. the type of this field is {@link Comparable}.
 *
 * @param <P> the field's (Runtime)-type.
 */
public abstract class BoundedField<P> extends Field {

  /**
   * The lower bound, i.e. each value-assignment to this {@code BoundedField} must satisfy {@code
   * newValue.compareTo(lowerBound) >= 0}.
   */
  @NotNull private final P lowerBound;

  /**
   * The upper bound, i.e. each value-assignment to this {@code BoundedField} must satisfy {@code
   * newValue.compareTo(lowerBound) <= 0}
   */
  @NotNull private final P upperBound;

  /** Specifies whether this {@code BoundedField} has a default value. */
  private final boolean hasDefaultValue;

  /** The default value assigned to this {@code BoundedField}. */
  @NotNull private final P defaultValue;

  /**
   * Constructor.
   *
   * @param type the type of the {@code BoundedField}.
   * @param name the {@code BoundedField}'s name.
   * @param lowerBound the lower bound.
   * @param upperBound the upper bound.
   * @param hasDefaultValue whether or not this field has a default value.
   * @param defaultValue the default value.
   * @param nullable whether {@code null}-assignment should be allowed or not.
   * @throws IllegalArgumentException if any reference-type argument is {@code null}.
   */
  protected BoundedField(
      @NotNull final TypeMirror type,
      @NotNull final CharSequence name,
      @NotNull final P lowerBound,
      @NotNull final P upperBound,
      final boolean hasDefaultValue,
      final P defaultValue,
      final boolean nullable)
      throws IllegalArgumentException {
    super(type, name, nullable);
    this.lowerBound = Objects.requireNonNullElseGet(lowerBound, Exceptions::illegalArgument);
    this.upperBound = Objects.requireNonNullElseGet(upperBound, Exceptions::illegalArgument);
    this.hasDefaultValue = hasDefaultValue;
    this.defaultValue = defaultValue;
  }

  public abstract boolean hasLowerBound();

  @NotNull
  public P lowerBound() {
    return this.lowerBound;
  }

  public abstract boolean hasUpperBound();

  @NotNull
  public P upperBound() {
    return this.upperBound;
  }

  public boolean hasBounds() {
    return (this.hasLowerBound() || this.hasUpperBound());
  }

  public P defaultValue() {
    return this.defaultValue;
  }

  public boolean hasDefaultValue() {
    return this.hasDefaultValue;
  }
}
