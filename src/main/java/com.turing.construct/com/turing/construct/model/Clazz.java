package com.turing.construct.model;

import com.turing.construct.Exceptions;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.NotNull;

/**
 * Representation of a generated builder-class.
 *
 * <p>As of now, {@link Clazz} has a name, a package (same as the class it is building), a set of
 * fields, methods and constructors.
 */
public class Clazz {

  /** Empty String, representing the default package. */
  public static final String NO_PACKAGE = "";

  /** The name of this {@code Clazz}. */
  @NotNull private final CharSequence name;

  /** The package of the {@code Clazz}. */
  @NotNull private final CharSequence packageName;

  /** The {@code Field}s within the {@code Clazz}. */
  @NotNull private final Set<Field> fields;

  /** The {@code Method}s within the {@code Clazz}. */
  @NotNull private final Set<Method> methods;

  /** The {@code Constructor}s of the {@code Clazz}. */
  @NotNull private final Set<Constructor> constructors;

  /**
   * Constructor.
   *
   * @param name the name of this {@code Clazz}.
   * @param packageName the package of the {@code Clazz}.
   * @throws IllegalArgumentException if argument {@code name} or {@code packageName} is {@code
   *     null}.
   */
  public Clazz(@NotNull final CharSequence name, @NotNull final CharSequence packageName)
      throws IllegalArgumentException {
    this.name = Objects.requireNonNullElseGet(name, Exceptions::illegalArgument);
    this.packageName = Objects.requireNonNullElseGet(packageName, Exceptions::illegalArgument);
    this.fields = new HashSet<>();
    this.methods = new HashSet<>();
    this.constructors = new HashSet<>();
  }

  /**
   * Constructor. The package will be set to {@link #NO_PACKAGE}.
   *
   * @param name the name of this {@code Clazz}.
   * @throws IllegalArgumentException if argument {@code name} is {@code null}.
   */
  public Clazz(@NotNull final CharSequence name) {
    this(name, Clazz.NO_PACKAGE);
  }

  @NotNull
  public String name() {
    return this.name.toString();
  }

  @NotNull
  public String packageName() {
    return this.packageName.toString();
  }

  /**
   * Adds a field (attribute) to this class.
   *
   * @param field to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if argument {@code field} is {@code null}.
   */
  @NotNull
  public Clazz addField(final Field field) {
    Objects.requireNonNullElseGet(field, Exceptions::illegalArgument);

    this.fields.add(field);
    return this;
  }

  /**
   * Adds fields (attributes) to this class.
   *
   * @param fields to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code fields} is {@code null}.
   */
  public Clazz addFields(final Field... fields) {
    return this.addFields(Arrays.asList(fields));
  }

  /**
   * Adds fields (attributes) to this class.
   *
   * @param fields to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code fields} is {@code null}.
   */
  public Clazz addFields(final Collection<Field> fields) throws IllegalArgumentException {
    Objects.requireNonNullElseGet(fields, Exceptions::illegalArgument);

    this.fields.addAll(fields);
    return this;
  }

  public Set<Field> fields() {
    return (Collections.unmodifiableSet(this.fields));
  }

  /**
   * Adds a (non-constructor) method to this class. For constructors, use{@link
   * #addConstructor(Constructor)}.
   *
   * @param method to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code method} is {@code null}.
   * @see #addConstructor(Constructor)
   */
  public Clazz addMethod(final Method method) {
    Objects.requireNonNullElseGet(method, Exceptions::illegalArgument);

    this.methods.add(method);
    return this;
  }

  /**
   * Adds (non-constructor) methods to this class. For constructors, use{@link
   * #addConstructors(Constructor...)}.
   *
   * @param methods to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code methods} is {@code null}.
   * @see #addConstructor(Constructor)
   */
  public Clazz addMethods(final Method... methods) {
    return this.addMethods(Arrays.asList(methods));
  }

  /**
   * Adds (non-constructor) methods to this class. For constructors, use{@link
   * #addConstructors(Collection)}.
   *
   * @param methods to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code methods} is {@code null}.
   * @see #addConstructor(Constructor)
   */
  public Clazz addMethods(final Collection<Method> methods) {
    Objects.requireNonNullElseGet(methods, Exceptions::illegalArgument);

    this.methods.addAll(methods);
    return this;
  }

  public Set<Method> methods() {
    return Collections.unmodifiableSet(this.methods);
  }

  /**
   * Adds a constructor to this class.
   *
   * @param constructor to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code constructor} is {@code null}.
   */
  public Clazz addConstructor(final Constructor constructor) {
    Objects.requireNonNullElseGet(constructor, Exceptions::illegalArgument);

    this.constructors.add(constructor);
    return this;
  }

  /**
   * Adds constructors to this class.
   *
   * @param constructors to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code constructors} is {@code null}.
   */
  public Clazz addConstructors(final Constructor... constructors) {
    return this.addConstructors(Arrays.asList(constructors));
  }

  /**
   * Adds constructors to this class.
   *
   * @param constructors to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code constructors} is {@code null}.
   */
  public Clazz addConstructors(final Collection<Constructor> constructors) {
    Objects.requireNonNullElseGet(constructors, Exceptions::illegalArgument);

    this.constructors.addAll(constructors);
    return this;
  }

  public Set<Constructor> constructors() {
    return Collections.unmodifiableSet(this.constructors);
  }
}
