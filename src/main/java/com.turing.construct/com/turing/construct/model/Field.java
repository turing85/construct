package com.turing.construct.model;

import com.turing.construct.Environment;
import com.turing.construct.Exceptions;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Types;
import javax.validation.constraints.NotNull;

/**
 * Representation of a class-field.
 *
 * <p>A field consists of its type, e.g. {@code int}, {@code java.lang.String}, its name and some
 * modifiers. It also specifies, if it is allowed to be {@code null}.
 *
 * <p>Since we use this class for compile-time types, we cannot use the actual types (i.e. instances
 * of {@link Class}, but use {@link TypeMirror}s to represent the type.
 */
public class Field {

  /** The type of the field. */
  @NotNull private final TypeMirror type;

  /** The field's name. */
  @NotNull private final CharSequence name;

  /** Specifies whether null-assignment should be allowed or not. */
  private final boolean nullable;

  /** The modifiers for this field. */
  @NotNull private final Set<Modifier> modifiers;

  /**
   * Constructor.
   *
   * @param type the type of the {@code Field}.
   * @param name the {@code Field}'s name.
   * @param nullable whether {@code null}-assignment should be allowed or not.
   * @throws IllegalArgumentException if argument {@code type} or {@code name} is {code null}.
   */
  public Field(final TypeMirror type, final CharSequence name, boolean nullable)
      throws IllegalArgumentException {
    this.type = Objects.requireNonNullElseGet(type, Exceptions::illegalArgument);
    this.name = Objects.requireNonNullElseGet(name, Exceptions::illegalArgument);
    this.nullable = nullable;
    this.modifiers = new HashSet<>();
  }

  @NotNull
  public String name() {
    return this.name.toString();
  }

  @NotNull
  public TypeMirror type() {
    return this.type;
  }

  public boolean isNullable() {
    return this.nullable;
  }

  /**
   * Adds a modifier to this field.
   *
   * @param modifier to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code modifier} is {@code null}.
   */
  @NotNull
  public Field addModifier(Modifier modifier) {
    this.modifiers.add(modifier);
    return this;
  }

  /**
   * Adds modifiers to this field.
   *
   * @param modifiers to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code modifiers} is {@code null}.
   */
  @NotNull
  public Field addModifiers(Modifier... modifiers) {
    return this.addModifiers(Arrays.asList(modifiers));
  }

  /**
   * Adds modifiers to this field.
   *
   * @param modifiers to add.
   * @return {@code this}, for method chaining.
   * @throws IllegalArgumentException if {@code modifiers} is {@code null}.
   */
  @NotNull
  public Field addModifiers(Collection<Modifier> modifiers) {
    Objects.requireNonNullElseGet(modifiers, Exceptions::illegalArgument);

    this.modifiers.addAll(modifiers);
    return this;
  }

  public Set<Modifier> modifiers() {
    return Collections.unmodifiableSet(this.modifiers);
  }

  /**
   * Searches for constructors containing a parameter with same name and in assignable form to this
   * field.
   *
   * <p>"In assignable form" is defined by calling {@link Types#isAssignable(TypeMirror,
   * TypeMirror)} with the type of this field as first and the type of the constructor parameter as
   * second argument.
   *
   * @param clazz The class to use as search space.
   * @return a {@code List} of all fitting constructors for this field.
   * @throws IllegalArgumentException if parameter {@code clazz} is {@code null}.
   */
  public List<ExecutableElement> fittingConstructors(final Element clazz)
      throws IllegalArgumentException {
    Objects.requireNonNullElseGet(clazz, Exceptions::illegalArgument);

    return ElementFilter.constructorsIn(clazz.getEnclosedElements())
        .stream()
        .filter(
            ctor ->
                ctor.getParameters()
                    .stream()
                    .anyMatch(
                        param ->
                            param.getSimpleName().toString().equals(this.name())
                                && Environment.getTypeUtils()
                                    .isAssignable(this.type, param.asType())))
        .collect(Collectors.toList());
  }

  /**
   * Searches for setters for this field.
   *
   * <p>A setter in this context is defined as a method {@code [modifiers]
   * set<fieldName>(<fieldType> someName)}.
   *
   * @param clazz The class to use as search space.
   * @return {@code true} if a setter is found, {@code false} otherwise.
   * @throws IllegalArgumentException if parameter {@code clazz} is {@code null}.
   */
  public boolean hasSetter(final Element clazz) throws IllegalArgumentException {
    Objects.requireNonNullElseGet(clazz, Exceptions::illegalArgument);

    return ElementFilter.methodsIn(clazz.getEnclosedElements())
        .stream()
        .anyMatch(
            method ->
                method
                    .getParameters()
                    .stream()
                    .anyMatch(
                        param ->
                            param.getSimpleName().toString().equals("set" + this.name())
                                && Environment.getTypeUtils()
                                    .isAssignable(this.type, param.asType())));
  }
}
