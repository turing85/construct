package com.turing.construct;

import java.util.Objects;
import java.util.function.Supplier;
import javax.validation.constraints.NotNull;

/**
 * Utility class used to let {@link Objects#requireNonNullElseGet(Object, Supplier)} throw an
 * exception.
 *
 * <p>Why this class? Because I hate throwing {@link NullPointerException}s. They should indicate
 * that some internal programming failure has occurred and thus indicate a critical bug. The
 * problem, however, is that {@link Objects#requireNonNull(Object)} always throws a {@link
 * NullPointerException}, if its parameter is {@code null}.
 *
 * <p>We can, however trick {@link Objects} by using {@link Objects#requireNonNullElseGet(Object,
 * Supplier)} by providing a {@link Supplier} that always throws an instance of an exception that we
 * want instead. This is why all the static methods have a generic return type. With this utility
 * class, one can write code like {@code Objects.requireNonNullOrElseGet(someObject,
 * Exceptions::illegalArgument);}.
 */
public class Exceptions {

  /**
   * Private constructor to deny instantiation of this class.
   *
   * @throws IllegalStateException always.
   */
  private Exceptions() throws IllegalStateException {
    throw new IllegalStateException("This class must not be instantiated.");
  }

  /**
   * When called, this method will always throw an {@link IllegalArgumentException}.
   *
   * @param <T> unused.
   * @return nothing.
   * @throws IllegalArgumentException always.
   */
  @NotNull
  public static <T> T illegalArgument() throws IllegalArgumentException {
    throw new IllegalArgumentException();
  }

  /**
   * When called, this method will always throw an {@link IllegalStateException}.
   *
   * @param <T> unused.
   * @return nothing.
   * @throws IllegalStateException always.
   */
  @NotNull
  public static <T> T illegalState() throws IllegalStateException {
    throw new IllegalStateException();
  }
}
