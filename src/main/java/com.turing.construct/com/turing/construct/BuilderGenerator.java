package com.turing.construct;

import com.turing.construct.annotation.Buildable;
import com.turing.construct.model.Clazz;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import com.turing.construct.processors.MethodGenerator;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.ServiceLoader.Provider;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.validation.constraints.NotNull;

/**
 * The {@code BuilderGenerator} is the entry point for the generation process.
 *
 * <p>This class mainly acts as initializer for data structures used throughout the generation
 * process and as an orchestrator to delegate field processing to the {@link FieldGenerator}s and
 * {@link MethodGenerator}s. Since every generator specifies, which annotation it processes, the
 * generation process is kept dynamic and extensible. Thus, new annotations and new processors (even
 * for already specified annotations) can be plugged in.
 */
public class BuilderGenerator extends AbstractProcessor {

  /**
   * The annotations processed by this processor.
   *
   * <p>This is the {@code Set} returned when {@link #getSupportedAnnotationTypes()} is called. It
   * is preferable to use the method-overwriting instead of {@link SupportedAnnotationTypes} since
   * this technique allows direct access to the annotation class and thus to {@link
   * Objects#getClass()}{@code .}{@link Class#getCanonicalName() getCannonicalName()}. This approach
   * guarantees that the annotations actually exist.
   */
  public static final Set<String> SUPPORTED_ANNOTATION_TYPES =
      Set.of(Buildable.class.getCanonicalName());

  /**
   * A list of all {@link FieldGenerator}s used to process annotated fields.
   *
   * <p>The list is ordered by the {@link FieldGenerator#priority priority priority} of the
   * generators. The generators are dynamically loaded at execution time via {@link
   * ServiceLoader#load(Class)}.
   *
   * @see #initGenerators()
   */
  @NotNull private List<FieldGenerator> fieldGenerators;

  /**
   * A list of all {@link MethodGenerator}s used to process annotated fields.
   *
   * <p>The list is ordered by the {@link MethodGenerator#priority} of the generators. The
   * generators are dynamically loaded at execution time via {@link ServiceLoader#load(Class)}.
   *
   * @see #initGenerators()
   */
  @NotNull private List<MethodGenerator> methodGenerators;

  /**
   * {@inheritDoc}
   *
   * @return an immutable Set of supported annotation types.
   */
  @Override
  public Set<String> getSupportedAnnotationTypes() {
    return BuilderGenerator.SUPPORTED_ANNOTATION_TYPES;
  }

  /** {@inheritDoc} */
  @Override
  public SourceVersion getSupportedSourceVersion() {
    return SourceVersion.RELEASE_9;
  }

  /**
   * {@inheritDoc}
   *
   * <p>This method also calls {@link #initGenerators()} to initialize the generators used to
   * generate the builder class.
   *
   * @param processingEnv the {@code ProcessingEnvironment} of the compilation process.
   * @throws IllegalArgumentException if {@code processingEnv} is {@code null}.
   */
  @Override
  public synchronized void init(final ProcessingEnvironment processingEnv)
      throws IllegalArgumentException {
    Objects.requireNonNullElseGet(processingEnv, Exceptions::illegalArgument);

    Environment.init(processingEnv);
    this.initGenerators();
    super.init(processingEnv);
  }

  /**
   * Loads the {@link FieldGenerator}s and {@link MethodGenerator}s.
   *
   * <p>The generators are loaded through {@link ServiceLoader#load(Class)}. The generators are
   * sorted by their priorities. This allows to effectively overwrite the behaviour of other
   * generators associated with the same annotation by registering the own generator with a higher
   * priority.
   *
   * @see FieldGenerator#priority
   * @see FieldGenerator#priority()
   * @see MethodGenerator#priority
   * @see MethodGenerator#priority()
   */
  private void initGenerators() {
    this.fieldGenerators =
        ServiceLoader.load(FieldGenerator.class)
            .stream()
            .map(Provider::get)
            .sorted()
            .collect(Collectors.toList());
    this.methodGenerators =
        ServiceLoader.load(MethodGenerator.class)
            .stream()
            .map(Provider::get)
            .sorted()
            .collect(Collectors.toList());
  }

  /**
   * {@inheritDoc}
   *
   * <p>The parameter {@code annotations} is not used in the current implementation. It is, however,
   * checked for {@code null} anyway in case future implementation changes and it is used.
   *
   * @throws IllegalArgumentException if argument {@code annotations} or {@code roundEnv} is {@code
   *     null}.
   */
  @Override
  public boolean process(
      @NotNull final Set<? extends TypeElement> annotations,
      @NotNull final RoundEnvironment roundEnv) {
    Objects.requireNonNullElseGet(annotations, Exceptions::illegalArgument);
    Objects.requireNonNullElseGet(roundEnv, Exceptions::illegalArgument);

    Set<? extends Element> buildables = roundEnv.getElementsAnnotatedWith(Buildable.class);
    Environment.getMessager()
        .debug(
            "Found the following classes: %s",
            buildables.stream().map(Element::asType).collect(Collectors.toList()));

    for (Element buildable : buildables) {
      if (this.isOfKind(buildable, ElementKind.CLASS) == false) {
        continue;
      }

      final String clazzName = buildable.getSimpleName() + "Builder";
      final String packageName =
          Environment.getProcessingEnv()
              .getElementUtils()
              .getPackageOf(buildable)
              .getQualifiedName()
              .toString();

      Clazz clazz = new Clazz(clazzName, packageName);

      List<Field> fields = new ArrayList<>();
      this.analyze(buildable, fields);

      for (final Field field : fields) {
        Environment.getMessager()
            .debug(
                "Constructors in %s satisfying %s %s: %s",
                buildable.asType(),
                Environment.getSimpleName(field.type()),
                field.name(),
                field.fittingConstructors(buildable));
      }

      try {
        this.generateMethods(buildable, fields, clazz);
      } catch (ProcessorException e) {
        // error has been logged before, continue with next class
        continue;
      }

      Environment.getMessager()
          .debug("Generated %s methods: %s", clazz.methods().size(), clazz.methods());
    }

    return true;
  }

  /**
   * Analyze a given {@link Element} and add its annotated fields to the given {@link List}.
   *
   * @param buildable the class to analyze.
   * @param fields the list where the annotated fields are added to.
   * @throws IllegalArgumentException if {@code buildable} or {@code fields} is {@code null}.
   */
  private void analyze(
      @NotNull final Element buildable, @NotNull final List<? super Field> fields) {
    Objects.requireNonNullElseGet(buildable, Exceptions::illegalArgument);
    Objects.requireNonNullElseGet(fields, Exceptions::illegalArgument);

    for (final FieldGenerator fieldGenerator : this.fieldGenerators) {
      try {
        fields.addAll(fieldGenerator.analyze(buildable));
      } catch (ProcessorException e) {
        // error has been logged before, continue with next class
        continue;
      }
    }
  }

  /**
   * Generate and store the methods to the given {@code fields} of the given {@link Element}
   * in the given {@link Clazz}.
   *
   * @param buildable a compile-time class.
   * @param fields to generate the methods for.
   * @param clazz the class where the generated methods are stored in.
   * @throws IllegalArgumentException if {@code buildable}, {@code fields} or {@code clazz} is
   *     {@code null}.
   * @throws ProcessorException if something goes horribly wrong.
   */
  private void generateMethods(
      @NotNull final Element buildable,
      @NotNull final List<? extends Field> fields,
      @NotNull final Clazz clazz)
      throws IllegalArgumentException, ProcessorException {
    Objects.requireNonNullElseGet(buildable, Exceptions::illegalArgument);
    Objects.requireNonNullElseGet(fields, Exceptions::illegalArgument);
    Objects.requireNonNullElseGet(clazz, Exceptions::illegalArgument);

    for (final Field field : fields) {
      for (final MethodGenerator methodGenerator : this.methodGenerators) {
        if (methodGenerator.accepts(field)) {
          clazz.addMethod(methodGenerator.constructMethod(clazz, field));
          break;
        }
        Environment.getMessager()
            .error(
                "%s -> no processor for %s %s",
                buildable.asType(), Environment.getSimpleName(field.type()), field.name());
      }
    }
  }
  /**
   * Checks whether the element is of the given kinds.
   *
   * <p>If it is of a different kind, an error message is printed via the {@link
   * Environment#getMessager()}.
   *
   * @param element element to check.
   * @param kind kind check criteria.
   * @return {@code true} if the element has one of the given kinds, {@code false} otherwise.
   * @see ElementKind
   */
  private boolean isOfKind(@NotNull final Element element, @NotNull final ElementKind kind) {
    Objects.requireNonNullElseGet(element, Exceptions::illegalArgument);
    Objects.requireNonNullElseGet(kind, Exceptions::illegalArgument);

    boolean isOfKind = true;
    if (element.getKind().equals(kind) == false) {
      Environment.getMessager()
          .error(
              "%s %s -> must be of a(n) %s",
              element.getKind().name().toLowerCase(),
              element.getSimpleName(),
              kind.name().toLowerCase());
      isOfKind = false;
    }
    return isOfKind;
  }
}
