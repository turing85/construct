package com.turing.construct;

import java.util.Objects;
import javax.annotation.processing.Messager;
import javax.tools.Diagnostic.Kind;
import javax.validation.constraints.NotNull;

/**
 * A wrapper class for {@link Messager}, providing convenient methods for printing error- and
 * debug-messages.
 */
public class SimpleMessager {

  /** The underlying {@code Messager} used. */
  @NotNull private final Messager messager;

  /**
   * Constructor.
   *
   * @param messager The underlying {@code Messager} to use.
   * @throws IllegalArgumentException if {@code messager} is {@code null}.
   */
  SimpleMessager(final Messager messager) throws IllegalArgumentException {
    this.messager = Objects.requireNonNullElseGet(messager, Exceptions::illegalArgument);
  }

  /**
   * Prints an error message.
   *
   * <p>Internally calls {@code this.messager.printMessage(Kind.ERROR, msg);}.
   *
   * @param msg error message.
   */
  public void error(String msg) {
    this.messager.printMessage(Kind.ERROR, msg);
  }

  /**
   * Prints an error message.
   *
   * <p>Internally calls {@code this.error(String.format(fmt, args));}.
   *
   * @param fmt a format string.
   * @param args the arguments for {@code fmt}.
   */
  public void error(String fmt, Object... args) {
    this.error(String.format(fmt, args));
  }

  /**
   * Prints a debug message.
   *
   * <p>Internally calls {@code this.messager.printMessage(Kind.OTHER, msg);}.
   *
   * @param msg error message.
   */
  public void debug(String msg) {
    this.messager.printMessage(Kind.OTHER, msg);
  }

  /**
   * Prints a debug message.
   *
   * <p>Internally calls {@code this.debug(String.format(fmt, args));}.
   *
   * @param fmt a format string.
   * @param args the arguments for {@code fmt}.
   */
  public void debug(String fmt, Object... args) {
    this.debug(String.format(fmt, args));
  }
}
