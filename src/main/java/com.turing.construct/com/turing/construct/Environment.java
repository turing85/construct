package com.turing.construct;

import java.util.Objects;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.validation.constraints.NotNull;

/**
 * Holds commonly used data structures and convenience methods.
 *
 * <p>The class is implemented as singleton. A single {@link #instance }is constructed through the
 * call to {@link #init(ProcessingEnvironment)}. All methods then use the instance to perform the
 * requested operation. Before this class can be used, {@link #init(ProcessingEnvironment)} must be
 * called. Otherwise the call to any method will throw an {@link IllegalStateException}.
 */
public final class Environment {

  /** The singleton instance. */
  private static Environment instance;

  /** The {@code ProcessingEnvironment} provided by the compiler. */
  @NotNull private final ProcessingEnvironment processingEnv;

  /**
   * A {@code SimpleMessager} constructed with the {@link Messager} provided by {@link
   * #processingEnv}{@code .}{@link ProcessingEnvironment#getMessager()}.
   */
  @NotNull private final SimpleMessager messager;

  /**
   * Private constructor for singleton construction.
   *
   * <p>The constructor call is not thread-safe and does not check whether it was called only once.
   * These checks are performed by {@link #init(ProcessingEnvironment)}.
   *
   * @param processingEnv the {@code ProcessingEnvironment} of this code generation process.
   * @throws IllegalArgumentException if {@code processingEnv} is {@code null}.
   */
  private Environment(@NotNull final ProcessingEnvironment processingEnv)
      throws IllegalArgumentException {
    this.processingEnv = Objects.requireNonNullElseGet(processingEnv, Exceptions::illegalArgument);
    this.messager = new SimpleMessager(processingEnv.getMessager());
  }

  /**
   * Initializes the environment by constructing the singleton.
   *
   * <p>Initialization is thread-safe. If this method was called before, the already constructed
   * {@link #instance} is not updated.
   *
   * @param processingEnv the {@code ProcessingEnvironment} provided by the compiler.
   * @throws IllegalArgumentException if {@code processingEnv} and {@link #instance} are both {@code
   *     null}.
   */
  public static synchronized void init(@NotNull final ProcessingEnvironment processingEnv)
      throws IllegalArgumentException {
    Objects.requireNonNullElseGet(processingEnv, Exceptions::illegalArgument);

    if (Environment.instance == null) {
      Environment.instance = new Environment(processingEnv);
    }
  }

  /**
   * @return {@link #processingEnv}
   * @throws IllegalStateException if {@link #init(ProcessingEnvironment)} has not yet been called.
   */
  @NotNull
  public static ProcessingEnvironment getProcessingEnv() throws IllegalStateException {
    Objects.requireNonNullElseGet(Environment.instance, Exceptions::illegalState);

    return Environment.instance.processingEnv;
  }

  /**
   * @return {@link #messager}
   * @throws IllegalStateException if {@link #init(ProcessingEnvironment)} has not yet been called.
   */
  @NotNull
  public static SimpleMessager getMessager() throws IllegalStateException {
    Objects.requireNonNullElseGet(Environment.instance, Exceptions::illegalState);

    return Environment.instance.messager;
  }

  /**
   * @return {@link #processingEnv}{@code}{@link ProcessingEnvironment#getTypeUtils()}
   * @throws IllegalStateException if {@link #init(ProcessingEnvironment)} has not yet been called.
   */
  public static Types getTypeUtils() throws IllegalStateException {
    return Environment.getProcessingEnv().getTypeUtils();
  }

  /**
   * @return {@link #processingEnv}{@code}{@link ProcessingEnvironment#getElementUtils()}
   * @throws IllegalStateException if {@link #init(ProcessingEnvironment)} has not yet been called.
   */
  public static Elements getElementUtils() throws IllegalStateException {
    return Environment.getProcessingEnv().getElementUtils();
  }

  /**
   * @return {@link #processingEnv}{@code}{@link ProcessingEnvironment#getFiler()}
   * @throws IllegalStateException if {@link #init(ProcessingEnvironment)} has not yet been called.
   */
  public static Filer getFiler() throws IllegalStateException {
    return Environment.getProcessingEnv().getFiler();
  }

  /**
   * Returns the simple name of a {@link TypeMirror}.
   *
   * <p>A simple name is the name without package- and class-information. For example, the simple
   * name of {@code java.lang.Long} is {@code "Long"}, whereas the simple name of {@code int} is
   * {@code "int"}.
   *
   * @param type the type to convert.
   * @return the simple name of {@code type}.
   * @throws IllegalArgumentException if argument {@code type} is {@code null}.
   * @throws IllegalStateException if {@link #init(ProcessingEnvironment)} has not yet been called.
   */
  @NotNull
  public static String getSimpleName(@NotNull final TypeMirror type)
      throws IllegalArgumentException, IllegalStateException {
    Objects.requireNonNullElseGet(type, Exceptions::illegalArgument);

    final String simpleName;
    if (type.getKind().isPrimitive()) {
      simpleName = type.toString();
    } else {
      simpleName =
          Environment.getElementUtils().getTypeElement(type.toString()).getSimpleName().toString();
    }
    return simpleName;
  }
}
