package com.turing.construct.processors;

import com.turing.construct.BuilderGenerator;
import com.turing.construct.Environment;
import com.turing.construct.Exceptions;
import com.turing.construct.ProcessorException;
import com.turing.construct.model.Field;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import javax.validation.constraints.NotNull;

/** This is a generic class to process field annotations. */
public abstract class FieldGenerator implements Comparable<FieldGenerator> {

  public static final int DEFAULT_PRIORITY = 100;

  /**
   * The priority of this generator.
   *
   * <p>The lower the priority of a generator, the earlier the generator is executed.
   */
  private final int priority;

  /** The annotation this generator processes. */
  @NotNull private final Class<? extends Annotation> annotation;

  /**
   * The type that the annotated field should have.
   *
   * @see #allOfExpectedType(Collection)
   */
  @NotNull private final TypeMirror expectedType;

  /**
   * Constructs a new FieldGenerator.
   *
   * @param annotation the annotation processed by this FieldGenerator.
   * @param expectedType the type that the annotated field should have. To be precise, {@code
   *     expectedType} must be {@link Types#isAssignable(TypeMirror, TypeMirror)} to all fields
   *     annotated with {@code annotation}.
   * @param priority the priority for this generator.
   * @throws IllegalArgumentException if argument {@code annotation} or {@code expectedType} is
   *     {@code null}.
   */
  protected FieldGenerator(
      @NotNull final Class<? extends Annotation> annotation,
      @NotNull final TypeMirror expectedType,
      @NotNull final int priority)
      throws IllegalArgumentException {
    this.annotation = Objects.requireNonNullElseGet(annotation, Exceptions::illegalArgument);
    this.expectedType = Objects.requireNonNullElseGet(expectedType, Exceptions::illegalArgument);
    this.priority = priority;
  }

  /**
   * Extracts all fields annotated with {@link #getAnnotation()} and forwards them to {@link
   * #model(List)}.
   *
   * <p>If {@link #getExpectedType()} is not {@link Types#isAssignable(TypeMirror, TypeMirror)} to
   * any field annotated with {@link #getAnnotation()}, an {@link ProcessorException} is thrown. The
   * actual calculation takes place in method {@link #model(List)}.
   *
   * @param rootClass a class annotated with {@code Buildable}.
   * @return a collection of {@code Field}s annotated with {@link #getAnnotation()}.
   * @throws IllegalArgumentException If {@code rootClass} is {@code null}.
   * @throws ProcessorException if {@link #expectedType} is not {@link
   *     Types#isAssignable(TypeMirror, TypeMirror)} to any field annotated with {@link
   *     #annotation}.
   * @see #model(List)
   */
  public Collection<Field> analyze(@NotNull final Element rootClass)
      throws IllegalArgumentException, ProcessorException {
    Objects.requireNonNullElseGet(rootClass, Exceptions::illegalArgument);

    final List<VariableElement> annotatedFields = this.filter(rootClass.getEnclosedElements());
    return this.model(annotatedFields);
  }

  /**
   * Processes the fields annotated with {@link #getAnnotation()}.
   *
   * <p>This is the actual generation-method. Each FieldGenerator is responsible to extract the
   * fields it fs responsible for, process the field and return a {@link Collection} of {@link
   * Field}s. The {@code Field}s are collected by the {@link BuilderGenerator} and then forwarded to
   * the {@link MethodGenerator}s.
   *
   * @param annotatedFields all fields annotated with {@link #getAnnotation()}.
   * @return a collection of {@code Field}s annotated with {@link #getAnnotation()}.
   * @throws IllegalArgumentException if {@code model} is {@code null}.
   */
  protected abstract Collection<Field> model(@NotNull List<VariableElement> annotatedFields)
      throws IllegalArgumentException;

  /**
   * Constructs a list of all elements with {@link ElementKind#FIELD} and annotated with {@link
   * #annotation}.
   *
   * <p>If an element is annotated with {@link #annotation}, but is not a field, an error message is
   * printed and a {@link ProcessorException} is thrown. If {@link #expectedType} is not {@link
   * Types#isAssignable(TypeMirror, TypeMirror)} to one of the fields, an error message is printed
   * and a {@link ProcessorException} is thrown.
   *
   * @param elements candidate elements.
   * @return a list, containing all fields annotated with {@link #annotation}.
   * @throws IllegalArgumentException if {@code elements} is {@code null}.
   * @throws ProcessorException if one element is annotated, but not a field or if {@link
   *     #expectedType} is not {@link Types#isAssignable(TypeMirror, TypeMirror)} to one of the
   *     field's type.
   */
  @NotNull
  public List<VariableElement> filter(@NotNull final List<? extends Element> elements)
      throws IllegalArgumentException, ProcessorException {
    Objects.requireNonNullElseGet(elements, Exceptions::illegalArgument);

    List<Element> candidates =
        elements
            .stream()
            .filter(element -> element.getAnnotation(this.annotation) != null)
            .collect(Collectors.toList());

    if (this.allFields(candidates) == false) {
      // no error printing needed, this was already done by the call to allFields(...)
      throw new ProcessorException();
    }

    if (this.allOfExpectedType(candidates) == false) {
      // no error printing needed, this was already done by the call to allFields(...)
      throw new ProcessorException();
    }

    return candidates
        .stream()
        .map(element -> (VariableElement) element)
        .collect(Collectors.toList());
  }

  /**
   * Checks whether each element is a field.
   *
   * <p>For each element of a different kind, an error message is printed via {@link
   * Environment#getMessager()}.
   *
   * @param elements collection to check.
   * @return {@code true} if each element ís a field, {@code false} otherwise.
   * @throws IllegalArgumentException if {@code elements} is {@code null}.
   */
  public boolean allFields(@NotNull final Collection<? extends Element> elements)
      throws IllegalArgumentException {
    Objects.requireNonNullElseGet(elements, Exceptions::illegalArgument);

    boolean allOk = true;
    for (Element element : elements) {
      if (element.getKind().equals(ElementKind.FIELD) == false) {
        Environment.getMessager()
            .error(
                "%s %s -> must be a %s",
                element.getKind().name().toLowerCase(),
                element.getSimpleName(),
                ElementKind.FIELD.name().toLowerCase());
        allOk = false;
      }
    }
    return allOk;
  }

  /**
   * Checks whether all elements in the Collection are type-assignable to {@link #expectedType}.
   *
   * <p>For each element that is not of the given type, an error message is printed via the {@link
   * Environment#getMessager()}.
   *
   * @param fields collection to check.
   * @return {@code true} if all elements are type-assignable, {@code false} otherwise.
   * @throws IllegalArgumentException if {@code fields} is {@code null}.
   * @see Types#isAssignable(TypeMirror, TypeMirror)
   */
  private boolean allOfExpectedType(@NotNull final Collection<? extends Element> fields) {
    Objects.requireNonNullElseGet(fields, Exceptions::illegalArgument);

    boolean allOk = true;
    for (Element field : fields) {
      final TypeMirror fieldType = field.asType();

      if (Environment.getTypeUtils().isAssignable(this.expectedType, fieldType) == false) {
        Element enclosing = field.getEnclosingElement();
        Environment.getMessager()
            .error(
                "In %s %s: %s %s; -> %s must be assignable to this field.",
                enclosing.getKind().name().toLowerCase(),
                enclosing.asType(),
                Environment.getSimpleName(fieldType),
                field.getSimpleName(),
                Environment.getSimpleName(this.expectedType));
        allOk = false;
      }
    }
    return allOk;
  }

  @NotNull
  public Class<? extends Annotation> getAnnotation() {
    return this.annotation;
  }

  @NotNull
  public TypeMirror getExpectedType() {
    return this.expectedType;
  }

  public final int priority() {
    return this.priority;
  }

  @Override
  public final int compareTo(@NotNull final FieldGenerator that) throws NullPointerException {
    // Throwing a NullPointerException} in order to comply to the contract of Comparable.
    Objects.requireNonNull(that);

    return this.priority() - that.priority();
  }

  @Override
  public final boolean equals(Object that) {
    if (that == null) {
      return false;
    }
    if ((that instanceof FieldGenerator) == false) {
      return false;
    }

    return this.priority() == ((FieldGenerator) that).priority();
  }

  @Override
  public final int hashCode() {
    return this.priority();
  }
}
