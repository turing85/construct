package com.turing.construct.processors;

import com.turing.construct.Exceptions;
import com.turing.construct.ProcessorException;
import com.turing.construct.model.Clazz;
import com.turing.construct.model.Field;
import com.turing.construct.model.Method;
import java.util.Objects;
import javax.validation.constraints.NotNull;

public abstract class MethodGenerator implements Comparable<MethodGenerator> {

  public static final int DEFAULT_PRIORITY = 100;

  /**
   * The priority of this generator.
   *
   * <p>The lower the priority of a generator, the earlier the generator is executed.
   */
  private final int priority;

  /** The type of field this generator generates {@link Method}s for. */
  @NotNull private final Class<? extends Field> generateFor;

  /**
   * Creates a method generator for the given field type.
   *
   * @param generateFor the field-type this method generator listens to.
   * @param priority the priority for this generator.
   * @throws IllegalArgumentException if argument {@code generateFor} is {@code null}.
   */
  protected MethodGenerator(@NotNull Class<? extends Field> generateFor, final int priority)
      throws IllegalArgumentException {
    this.generateFor = Objects.requireNonNullElseGet(generateFor, Exceptions::illegalArgument);
    this.priority = priority;
  }

  @NotNull
  public Class<? extends Field> generateFor() {
    return this.generateFor;
  }

  /**
   * Constructs a {@link Method} for {@code forField} in {@code OriginClass}.
   *
   * <p>This method constructs an object, representing a method {@code public <inClass>
   * with<forFieldName>(<forFieldType> <forFieldName>)}.
   *
   * @param inClass the class, in which the generated method should reside.
   * @param forField the field itself.
   * @return the suggested method.
   * @throws ProcessorException if something goes horribly wrong.
   */
  @NotNull
  public abstract Method constructMethod(Clazz inClass, Field forField) throws ProcessorException;

  /**
   * Inquiry-method to determine whether this generator is responsible for the given field.
   *
   * <p>The generator is responsible if the class of {@code field} is equal to {@link
   * #generateFor()}.
   *
   * @param field the field to check responsibility for.
   * @return {@code true}, if this generator is responsible, {@code false} otherwise.
   */
  public boolean accepts(@NotNull Field field) {
    Objects.requireNonNullElseGet(field, Exceptions::illegalArgument);

    return field.getClass().equals(this.generateFor());
  }

  public int priority() {
    return this.priority;
  }

  @Override
  public final int compareTo(@NotNull final MethodGenerator that) {
    // Throwing a NullPointerException} in order to comply to the contract of Comparable.
    Objects.requireNonNull(that);

    return this.priority() - that.priority();
  }

  @Override
  public final boolean equals(Object that) {
    if (that == null) {
      return (false);
    }
    if ((that instanceof MethodGenerator) == false) {
      return (false);
    }

    return (this.priority() == ((MethodGenerator) that).priority());
  }

  @Override
  public final int hashCode() {
    return this.priority();
  }
}
