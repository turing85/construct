package com.turing.construct.impl.annotations.primitives;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for {@code long}- and {@link Long}-fields.
 *
 * <p>If the class or one of its derived classes is annotated with {@link
 * com.turing.construct.annotation.Buildable}, a {@code public void with<fieldName>(long
 * <fieldName>)} method will be created.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface LongBuildProperty {

  /**
   * Lower bound for the field.
   *
   * <p>If the corresponding {@code with...(...)} method is called with an argument {@code <
   * lowerBound]}, an exception is thrown. The exception type is specified by {@link
   * #boundsException()}.
   *
   * @return the lower bound for the annotated field.
   */
  long lowerBound() default Long.MIN_VALUE;

  /**
   * Upper bound for the field.
   *
   * <p>If the corresponding {@code with...(...)} method is called with an argument {@code >
   * lowerBound]}, an exception is thrown. The exception type is specified by {@link
   * #boundsException()}.
   *
   * @return the upper bound for the annotated field.
   */
  long upperBound() default Long.MAX_VALUE;

  /**
   * Specifies whether the field should have a default value.
   *
   * <p>If it has a default value, the Builder will not enforce that this field must explicitly be
   * set by calling {@code with<fieldName>(long <fieldName>)}.
   *
   * <p>If the field does not have a default value and {@code build()} is called before the
   * corresponding {@code with<fieldName>(long <fieldName>)} method has been called, an exception is
   * thrown. The exception type is specified by {@link #buildException()}.
   *
   * @return {@code true} if the field has a default value, {@code false} otherwise.
   */
  boolean hasDefaultValue() default true;

  /**
   * The default value for this field.
   *
   * <p>This is only evaluated, if {@link #hasDefaultValue()} is set to {@code true}.
   *
   * @return the default value for the annotated field.
   */
  long defaultValue() default 0L;

  /**
   * The exception thrown if a {@link #lowerBound()} or {@link #upperBound()} bound is specified and
   * {@code with<fieldName>(long <fieldName>)} is called with a parameter outside of these bounds.
   *
   * @return exception-type thrown if bounds are violated.
   * @see #lowerBound()
   * @see #upperBound()
   */
  Class<? extends Exception> boundsException() default RuntimeException.class;

  /**
   * The exception thrown if no {@link #hasDefaultValue()} is set to {@code false } and {@code
   * build()} is called before {@code with<fieldName>(long <fieldName>)} has been called.
   *
   * @return exception-type thrown if object is constructed prematurely.
   * @see #hasDefaultValue()
   */
  Class<? extends Exception> buildException() default IllegalStateException.class;
}
