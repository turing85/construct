package com.turing.construct.impl.processors.fields.wrappers;

import com.turing.construct.impl.annotations.wrappers.CharacterWrapperBuildProperty;
import com.turing.construct.impl.model.fields.wrappers.CharacterWrapperField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;
import javax.validation.constraints.NotNull;

/**
 * Generator for fields annotated with {@link CharacterWrapperBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link CharacterWrapperField}s.
 */
public class CharacterWrapperGenerator extends FieldGenerator {

  /** Constructor. */
  public CharacterWrapperGenerator() {
    super(
        CharacterWrapperBuildProperty.class,
        CharacterWrapperField.MIRROR,
        FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> characterWrapperFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      CharacterWrapperBuildProperty annotation =
          field.getAnnotation(CharacterWrapperBuildProperty.class);
      boolean isNullable = (field.getAnnotation(NotNull.class) == null);
      characterWrapperFields.add(
          new CharacterWrapperField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue(),
              isNullable));
    }
    return characterWrapperFields;
  }
}
