package com.turing.construct.impl.processors.fields.primitives;

import com.turing.construct.impl.annotations.primitives.LongBuildProperty;
import com.turing.construct.impl.model.fields.primitives.LongField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;

/**
 * Generator for fields annotated with {@link LongBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link LongField}s.
 */
public class LongGenerator extends FieldGenerator {

  /** Constructor. */
  public LongGenerator() {
    super(LongBuildProperty.class, LongField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> longFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      LongBuildProperty annotation = field.getAnnotation(LongBuildProperty.class);
      longFields.add(
          new LongField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue()));
    }

    return longFields;
  }
}
