package com.turing.construct.impl.model.fields.primitives;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@code long}. */
public class LongField extends PrimitiveOrWrapperField<Long> {

  /** {@link TypeMirror} for the primitive {@code long}. */
  public static final TypeMirror MIRROR =
      Environment.getTypeUtils().getPrimitiveType(TypeKind.LONG);

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param lowerBound lowerBound the lower bound.
   * @param upperBound upperBound the upper bound.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @throws IllegalArgumentException if {@code name} is {@code null}.
   */
  public LongField(
      @NotNull final CharSequence name,
      final long lowerBound,
      final long upperBound,
      final boolean hasDefaultValue,
      final long defaultValue)
      throws IllegalArgumentException {
    super(LongField.MIRROR, name, lowerBound, upperBound, hasDefaultValue, defaultValue);
  }

  @Override
  public boolean hasLowerBound() {
    return this.lowerBound() != Long.MIN_VALUE;
  }

  @Override
  public boolean hasUpperBound() {
    return this.upperBound() != Long.MAX_VALUE;
  }
}
