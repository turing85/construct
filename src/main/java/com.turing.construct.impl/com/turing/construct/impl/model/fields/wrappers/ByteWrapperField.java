package com.turing.construct.impl.model.fields.wrappers;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import java.util.Objects;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@link Byte}. */
public class ByteWrapperField extends PrimitiveOrWrapperField<Byte> {

  /** {@link TypeMirror} for the primitive {@link Byte}. */
  public static final TypeMirror MIRROR =
      Environment.getElementUtils().getTypeElement(Byte.class.getCanonicalName()).asType();

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param lowerBound lowerBound the lower bound. May be {@code null}. If set to {@code null}, will
   *     be replaces with {@link Byte#MIN_VALUE}.
   * @param upperBound upperBound the upper bound. May be {@code null}. If set to {@code null}, will
   *     be replaces with {@link Byte#MAX_VALUE}.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @param nullable whether this field should allow {@code null}-assignment.
   * @throws IllegalArgumentException if {@code name} is {@code null}.
   */
  public ByteWrapperField(
      @NotNull final CharSequence name,
      final Byte lowerBound,
      final Byte upperBound,
      final boolean hasDefaultValue,
      final Byte defaultValue,
      final boolean nullable)
      throws IllegalArgumentException {
    super(
        ByteWrapperField.MIRROR,
        name,
        Objects.requireNonNullElse(lowerBound, Byte.MIN_VALUE),
        Objects.requireNonNullElse(upperBound, Byte.MAX_VALUE),
        hasDefaultValue,
        defaultValue,
        nullable);
  }

  @Override
  public boolean hasLowerBound() {
    return this.lowerBound() != Byte.MIN_VALUE;
  }

  @Override
  public boolean hasUpperBound() {
    return this.upperBound() != Byte.MAX_VALUE;
  }
}
