package com.turing.construct.impl.model.fields.wrappers;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import java.util.Objects;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@link Integer}. */
public class IntegerWrapperField extends PrimitiveOrWrapperField<Integer> {

  /** {@link TypeMirror} for the primitive {@link Integer}. */
  public static final TypeMirror MIRROR =
      Environment.getElementUtils().getTypeElement(Integer.class.getCanonicalName()).asType();

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param lowerBound lowerBound the lower bound. May be {@code null}. If set to {@code null}, will
   *     be replaces with {@link Integer#MIN_VALUE}.
   * @param upperBound upperBound the upper bound. May be {@code null}. If set to {@code null}, will
   *     be replaces with {@link Integer#MAX_VALUE}.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @param nullable whether this field should allow {@code null}-assignment.
   * @throws IllegalArgumentException if {@code name} is {@code null}.
   */
  public IntegerWrapperField(
      @NotNull final CharSequence name,
      final Integer lowerBound,
      final Integer upperBound,
      final boolean hasDefaultValue,
      final Integer defaultValue,
      final boolean nullable)
      throws IllegalArgumentException {
    super(
        IntegerWrapperField.MIRROR,
        name,
        lowerBound,
        upperBound,
        hasDefaultValue,
        Objects.requireNonNullElse(defaultValue, 0),
        nullable);
  }

  @Override
  public boolean hasLowerBound() {
    return this.lowerBound() != Integer.MIN_VALUE;
  }

  @Override
  public boolean hasUpperBound() {
    return this.upperBound() != Integer.MAX_VALUE;
  }
}
