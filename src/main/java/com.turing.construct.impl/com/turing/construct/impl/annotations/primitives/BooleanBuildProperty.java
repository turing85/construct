package com.turing.construct.impl.annotations.primitives;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for {@code boolean}- and {@link Boolean} fields.
 *
 * <p>If the class or one of its derived classes is annotated with {@link
 * com.turing.construct.annotation.Buildable}, a {@code public void with<fieldName>(boolean
 * <fieldName>)} method will be created.
 *
 * <p>In contrast to the other primitive-annotations, this annotation does not define lower/upper
 * bounds since they do not make sense in the context of {@code boolean}s.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface BooleanBuildProperty {

  /**
   * Specifies whether the field should have a default value.
   *
   * <p>If it has a default value, the Builder will not enforce that this field must explicitly be
   * set by calling {@code with<fieldName>(boolean <fieldName>)}.
   *
   * <p>If the field does not have a default value and {@code build()} is called before the
   * corresponding {@code with<fieldName>(boolean <fieldName>)} method has been called, an exception
   * is thrown. The exception type is specified by {@link #buildException()}.
   *
   * @return {@code true} if the field has a default value, {@code false} otherwise.
   */
  boolean hasDefaultValue() default true;

  /**
   * The default value for this field.
   *
   * <p>This is only evaluated, if {@link #hasDefaultValue()} is set to {@code true}.
   *
   * @return the default value for the annotated field.
   */
  boolean defaultValue() default false;

  /**
   * The exception thrown if no {@link #hasDefaultValue()} is set to {@code false } and {@code
   * build()} is called before {@code with<fieldName>(byte <fieldName>)} has been called.
   *
   * @return exception-type thrown if object is constructed prematurely.
   * @see #hasDefaultValue()
   */
  Class<? extends Exception> buildException() default IllegalStateException.class;
}
