package com.turing.construct.impl.processors.fields.primitives;

import com.turing.construct.impl.annotations.primitives.ShortBuildProperty;
import com.turing.construct.impl.model.fields.primitives.ShortField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;

/**
 * Generator for fields annotated with {@link ShortBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link ShortField}s.
 */
public class ShortGenerator extends FieldGenerator {

  /** Constructor. */
  public ShortGenerator() {
    super(ShortBuildProperty.class, ShortField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> shortFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      ShortBuildProperty annotation = field.getAnnotation(ShortBuildProperty.class);
      shortFields.add(
          new ShortField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue()));
    }

    return shortFields;
  }
}
