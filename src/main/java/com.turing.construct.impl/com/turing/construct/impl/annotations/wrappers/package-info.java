/**
 * Annotations for Wrappers.
 *
 * <p>Like primitives, the wrappers are a little bit special. Most importantly, autoboxing allows
 * the proper definition of default values, as well as bounds.
 *
 * <p>As with {@link com.turing.construct.impl.annotations.ReferenceBuildProperty}, this annotation
 * can be combined with {@link javax.validation.constraints.NotNull} to disallow {@code
 * null}-values.
 *
 * <p><b>Implementation notice:</b> This annotation is allowed on all types that are in assignable
 * form to the Wrapper. This also means that one could annotate, e.g. an {@code Object} with e.g.
 * {@code IntegerWrapperBuildProperty}. The use of such techniques is highly discouraged.
 */
package com.turing.construct.impl.annotations.wrappers;
