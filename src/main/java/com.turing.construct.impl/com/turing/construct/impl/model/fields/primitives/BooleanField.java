package com.turing.construct.impl.model.fields.primitives;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@code boolean}. */
public class BooleanField extends PrimitiveOrWrapperField<Boolean> {

  /** {@link TypeMirror} for the primitive {@code boolean}. */
  public static final TypeMirror MIRROR =
      Environment.getTypeUtils().getPrimitiveType(TypeKind.BOOLEAN);

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @throws IllegalArgumentException if {@code name} is {@code null}.
   */
  public BooleanField(
      @NotNull final CharSequence name, final boolean hasDefaultValue, final boolean defaultValue)
      throws IllegalArgumentException {
    super(BooleanField.MIRROR, name, false, true, hasDefaultValue, defaultValue);
  }

  @Override
  public boolean hasLowerBound() {
    return false;
  }

  @Override
  public boolean hasUpperBound() {
    return false;
  }
}
