package com.turing.construct.impl.model.fields;

import com.turing.construct.model.BoundedField;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

public abstract class PrimitiveOrWrapperField<P> extends BoundedField<P> {

  protected PrimitiveOrWrapperField(
      @NotNull final TypeMirror type,
      @NotNull final CharSequence name,
      @NotNull final P lowerBound,
      @NotNull final P upperBound,
      final boolean hasDefaultValue,
      @NotNull final P defaultValue)
      throws IllegalArgumentException {
    this(type, name, lowerBound, upperBound, hasDefaultValue, defaultValue, false);
  }

  protected PrimitiveOrWrapperField(
      @NotNull final TypeMirror type,
      @NotNull final CharSequence name,
      @NotNull final P lowerBound,
      @NotNull final P upperBound,
      final boolean hasDefaultValue,
      final P defaultValue,
      final boolean nullable)
      throws IllegalArgumentException {
    super(type, name, lowerBound, upperBound, hasDefaultValue, defaultValue, nullable);
  }
}
