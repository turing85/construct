package com.turing.construct.impl.processors.fields.primitives;

import com.turing.construct.impl.annotations.primitives.ByteBuildProperty;
import com.turing.construct.impl.model.fields.primitives.ByteField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.lang.model.element.VariableElement;

/**
 * Generator for fields annotated with {@link ByteBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link ByteField}s.
 */
public class ByteGenerator extends FieldGenerator {

  /** Constructor. */
  public ByteGenerator() {
    super(ByteBuildProperty.class, ByteField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected Collection<Field> model(final List<VariableElement> annotated) {
    List<Field> byteFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      ByteBuildProperty annotation = field.getAnnotation(ByteBuildProperty.class);
      byteFields.add(
          new ByteField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue()));
    }

    return byteFields;
  }
}
