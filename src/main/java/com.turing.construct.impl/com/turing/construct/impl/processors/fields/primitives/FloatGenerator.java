package com.turing.construct.impl.processors.fields.primitives;

import com.turing.construct.impl.annotations.primitives.FloatBuildProperty;
import com.turing.construct.impl.model.fields.primitives.FloatField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;

/**
 * Generator for fields annotated with {@link FloatBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link FloatField}s.
 */
public class FloatGenerator extends FieldGenerator {

  /** Constructor. */
  public FloatGenerator() {
    super(FloatBuildProperty.class, FloatField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> floatFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      FloatBuildProperty annotation = field.getAnnotation(FloatBuildProperty.class);
      floatFields.add(
          new FloatField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue()));
    }

    return floatFields;
  }
}
