package com.turing.construct.impl.processors.fields.wrappers;

import com.turing.construct.impl.annotations.wrappers.FloatWrapperBuildProperty;
import com.turing.construct.impl.model.fields.wrappers.FloatWrapperField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;
import javax.validation.constraints.NotNull;

/**
 * Generator for fields annotated with {@link FloatWrapperBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link FloatWrapperField}s.
 */
public class FloatWrapperGenerator extends FieldGenerator {

  /** Constructor. */
  public FloatWrapperGenerator() {
    super(
        FloatWrapperBuildProperty.class, FloatWrapperField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> floatWrapperFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      FloatWrapperBuildProperty annotation = field.getAnnotation(FloatWrapperBuildProperty.class);
      boolean isNullable = (field.getAnnotation(NotNull.class) == null);
      floatWrapperFields.add(
          new FloatWrapperField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue(),
              isNullable));
    }

    return floatWrapperFields;
  }
}
