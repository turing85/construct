package com.turing.construct.impl.model.fields.wrappers;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import java.util.Objects;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@link Boolean}. */
public class BooleanWrapperField extends PrimitiveOrWrapperField<Boolean> {

  /** {@link TypeMirror} for the primitive {@link Boolean}. */
  public static final TypeMirror MIRROR =
      Environment.getElementUtils().getTypeElement(Boolean.class.getCanonicalName()).asType();

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @param nullable whether this field should allow {@code null}-assignment.
   * @throws IllegalArgumentException if argument {@code name} is {@code null}.
   */
  public BooleanWrapperField(
      @NotNull final CharSequence name,
      final boolean hasDefaultValue,
      final Boolean defaultValue,
      final boolean nullable)
      throws IllegalArgumentException {
    super(
        BooleanWrapperField.MIRROR,
        name,
        false,
        true,
        hasDefaultValue,
        Objects.requireNonNullElse(defaultValue, false),
        nullable);
  }

  @Override
  public boolean hasLowerBound() {
    return false;
  }

  @Override
  public boolean hasUpperBound() {
    return false;
  }
}
