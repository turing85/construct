package com.turing.construct.impl.model.fields.wrappers;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import java.util.Objects;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@link Float}. */
public class FloatWrapperField extends PrimitiveOrWrapperField<Float> {

  /** {@link TypeMirror} for the primitive {@link Float}. */
  public static final TypeMirror MIRROR =
      Environment.getElementUtils().getTypeElement(Float.class.getCanonicalName()).asType();

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param lowerBound lowerBound the lower bound. May be {@code null}. If set to {@code null}, will
   *     be replaces with {@link Float#NEGATIVE_INFINITY}.
   * @param upperBound upperBound the upper bound. May be {@code null}. If set to {@code null}, will
   *     be replaces with {@link Float#POSITIVE_INFINITY}.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @param nullable whether this field should allow {@code null}-assignment.
   * @throws IllegalArgumentException if {@code name} is {@code null}.
   */
  public FloatWrapperField(
      @NotNull final CharSequence name,
      final Float lowerBound,
      final Float upperBound,
      final boolean hasDefaultValue,
      final Float defaultValue,
      final boolean nullable)
      throws IllegalArgumentException {
    super(
        FloatWrapperField.MIRROR,
        name,
        lowerBound,
        upperBound,
        hasDefaultValue,
        Objects.requireNonNullElse(defaultValue, 0.0f),
        nullable);
  }

  @Override
  public boolean hasLowerBound() {
    return this.lowerBound() != Float.NEGATIVE_INFINITY;
  }

  @Override
  public boolean hasUpperBound() {
    return this.upperBound() != Float.POSITIVE_INFINITY;
  }
}
