package com.turing.construct.impl.model.fields.primitives;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@code short}. */
public class ShortField extends PrimitiveOrWrapperField<Short> {

  public static final TypeMirror MIRROR =
      Environment.getTypeUtils().getPrimitiveType(TypeKind.SHORT);

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param lowerBound lowerBound the lower bound.
   * @param upperBound upperBound the upper bound.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @throws IllegalArgumentException if {@code name} is {@code null}.
   */
  public ShortField(
      @NotNull final CharSequence name,
      final short lowerBound,
      final short upperBound,
      final boolean hasDefaultValue,
      final short defaultValue)
      throws IllegalArgumentException {
    super(ShortField.MIRROR, name, lowerBound, upperBound, hasDefaultValue, defaultValue);
  }

  @Override
  public boolean hasLowerBound() {
    return this.lowerBound() != Short.MIN_VALUE;
  }

  @Override
  public boolean hasUpperBound() {
    return this.upperBound() != Short.MAX_VALUE;
  }
}
