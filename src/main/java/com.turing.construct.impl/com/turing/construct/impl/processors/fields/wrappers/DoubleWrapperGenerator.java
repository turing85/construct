package com.turing.construct.impl.processors.fields.wrappers;

import com.turing.construct.impl.annotations.wrappers.DoubleWrapperBuildProperty;
import com.turing.construct.impl.model.fields.wrappers.DoubleWrapperField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;
import javax.validation.constraints.NotNull;

/**
 * Generator for fields annotated with {@link DoubleWrapperBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link DoubleWrapperField}s.
 */
public class DoubleWrapperGenerator extends FieldGenerator {

  /** Constructor. */
  public DoubleWrapperGenerator() {
    super(
        DoubleWrapperBuildProperty.class,
        DoubleWrapperField.MIRROR,
        FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> doubleWrapperFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      DoubleWrapperBuildProperty annotation = field.getAnnotation(DoubleWrapperBuildProperty.class);
      boolean isNullable = (field.getAnnotation(NotNull.class) == null);
      doubleWrapperFields.add(
          new DoubleWrapperField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue(),
              isNullable));
    }
    return doubleWrapperFields;
  }
}
