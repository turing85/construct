package com.turing.construct.impl.model.fields.wrappers;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import java.util.Objects;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@link Character}. */
public class CharacterWrapperField extends PrimitiveOrWrapperField<Character> {

  /** {@link TypeMirror} for the primitive {@link Character}. */
  public static final TypeMirror MIRROR =
      Environment.getElementUtils().getTypeElement(Character.class.getCanonicalName()).asType();

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param lowerBound lowerBound the lower bound. May be {@code null}. If set to {@code null}, will
   *     be replaces with {@link Character#MIN_VALUE}.
   * @param upperBound upperBound the upper bound. May be {@code null}. If set to {@code null}, will
   *     be replaces with {@link Character#MAX_VALUE}.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @param nullable whether this field should allow {@code null}-assignment.
   * @throws IllegalArgumentException if {@code name} is {@code null}.
   */
  public CharacterWrapperField(
      @NotNull final CharSequence name,
      final Character lowerBound,
      final Character upperBound,
      final boolean hasDefaultValue,
      final Character defaultValue,
      final boolean nullable)
      throws IllegalArgumentException {
    super(
        CharacterWrapperField.MIRROR,
        name,
        lowerBound,
        upperBound,
        hasDefaultValue,
        Objects.requireNonNullElse(defaultValue, ((char) 0)),
        nullable);
  }

  @Override
  public boolean hasLowerBound() {
    return this.lowerBound() != Character.MIN_VALUE;
  }

  @Override
  public boolean hasUpperBound() {
    return this.upperBound() != Character.MAX_VALUE;
  }
}
