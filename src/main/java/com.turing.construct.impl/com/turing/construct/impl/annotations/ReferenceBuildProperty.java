package com.turing.construct.impl.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for fields (attributes) that should be handled/set by the builder.
 *
 * <p>This is a template-annotation for attributes the builder should be aware of. While every
 * {@link com.turing.construct.processors.FieldGenerator }defines which annotation it processes,
 * this is a prototype-annotation for the annotations provided with the framework.
 *
 * <p>This annotation can be combined with {@link javax.validation.constraints.NotNull} to enforce
 * that a reference is never set to {@code null}. By annotating a reference-type with both {@link
 * javax.validation.constraints.NotNull} and this annotation will also enforce that the
 * corresponding {@code with<fieldName>(...)}-method in the constructed builder must be called with
 * a non-{@code null} value before {@code build()} an instance can be created. If you write a custom
 * processor for this annotation, please keep the {@code NotNull} annotation in mind.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface ReferenceBuildProperty {}
