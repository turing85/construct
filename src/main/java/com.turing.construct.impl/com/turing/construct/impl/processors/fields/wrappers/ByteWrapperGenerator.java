package com.turing.construct.impl.processors.fields.wrappers;

import com.turing.construct.impl.annotations.wrappers.ByteWrapperBuildProperty;
import com.turing.construct.impl.model.fields.wrappers.ByteWrapperField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;
import javax.validation.constraints.NotNull;

/**
 * Generator for fields annotated with {@link ByteWrapperBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link ByteWrapperField}s.
 */
public class ByteWrapperGenerator extends FieldGenerator {

  /** Constructor. */
  public ByteWrapperGenerator() {
    super(ByteWrapperBuildProperty.class, ByteWrapperField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> byteWrapperFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      ByteWrapperBuildProperty annotation = field.getAnnotation(ByteWrapperBuildProperty.class);
      boolean isNullable = (field.getAnnotation(NotNull.class) == null);
      byteWrapperFields.add(
          new ByteWrapperField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue(),
              isNullable));
    }
    return byteWrapperFields;
  }
}
