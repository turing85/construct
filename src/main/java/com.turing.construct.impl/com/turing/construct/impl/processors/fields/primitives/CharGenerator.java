package com.turing.construct.impl.processors.fields.primitives;

import com.turing.construct.impl.annotations.primitives.CharBuildProperty;
import com.turing.construct.impl.model.fields.primitives.CharField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;

/**
 * Generator for fields annotated with {@link CharBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link CharField}s.
 */
public class CharGenerator extends FieldGenerator {

  /** Constructor. */
  public CharGenerator() {
    super(CharBuildProperty.class, CharField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> charFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      CharBuildProperty annotation = field.getAnnotation(CharBuildProperty.class);
      charFields.add(
          new CharField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue()));
    }

    return charFields;
  }
}
