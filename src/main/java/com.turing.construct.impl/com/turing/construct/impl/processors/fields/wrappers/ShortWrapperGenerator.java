package com.turing.construct.impl.processors.fields.wrappers;

import com.turing.construct.impl.annotations.wrappers.ShortWrapperBuildProperty;
import com.turing.construct.impl.model.fields.wrappers.ShortWrapperField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;
import javax.validation.constraints.NotNull;

/**
 * Generator for fields annotated with {@link ShortWrapperBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link ShortWrapperField}s.
 */
public class ShortWrapperGenerator extends FieldGenerator {

  /** Constructor. */
  public ShortWrapperGenerator() {
    super(
        ShortWrapperBuildProperty.class, ShortWrapperField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> shortWrapperFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      ShortWrapperBuildProperty annotation = field.getAnnotation(ShortWrapperBuildProperty.class);
      boolean isNullable = (field.getAnnotation(NotNull.class) == null);
      shortWrapperFields.add(
          new ShortWrapperField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue(),
              isNullable));
    }
    return shortWrapperFields;
  }
}
