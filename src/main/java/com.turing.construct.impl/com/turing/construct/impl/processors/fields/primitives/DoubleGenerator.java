package com.turing.construct.impl.processors.fields.primitives;

import com.turing.construct.impl.annotations.primitives.DoubleBuildProperty;
import com.turing.construct.impl.model.fields.primitives.DoubleField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;

/**
 * Generator for fields annotated with {@link DoubleBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link DoubleField}s.
 */
public class DoubleGenerator extends FieldGenerator {

  /** Constructor. */
  public DoubleGenerator() {
    super(DoubleBuildProperty.class, DoubleField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> doubleFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      DoubleBuildProperty annotation = field.getAnnotation(DoubleBuildProperty.class);
      doubleFields.add(
          new DoubleField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue()));
    }

    return doubleFields;
  }
}
