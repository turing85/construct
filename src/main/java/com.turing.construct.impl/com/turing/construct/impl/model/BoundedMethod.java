package com.turing.construct.impl.model;

import com.turing.construct.model.BoundedField;
import com.turing.construct.model.Clazz;
import com.turing.construct.model.Method;
import javax.validation.constraints.NotNull;

public class BoundedMethod<T> extends Method {

  public BoundedMethod(@NotNull final Clazz inClass, @NotNull final BoundedField<T> forField)
      throws IllegalArgumentException {
    super(inClass, forField);
  }

  @SuppressWarnings("unchecked")
  @Override
  @NotNull
  public BoundedField<T> forField() {
    return ((BoundedField<T>) super.forField());
  }

  @Override
  public String toString() {
    StringBuilder representation = new StringBuilder();
    representation.append(this.name());

    BoundedField<T> field = this.forField();
    if (field.hasBounds()) {
      representation.append(" ");

      if (field.hasLowerBound()) {
        representation.append("[").append(field.lowerBound());
      } else {
        representation.append("( ");
      }

      representation.append(",");

      if (field.hasUpperBound()) {
        representation.append(field.upperBound()).append("]");
      } else {
        representation.append(" )");
      }
    }

    if (field.hasDefaultValue()) {
      representation.append(" : ").append(field.defaultValue());
    }

    return representation.toString();
  }
}
