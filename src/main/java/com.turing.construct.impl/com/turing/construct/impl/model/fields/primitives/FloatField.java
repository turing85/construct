package com.turing.construct.impl.model.fields.primitives;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@code float}. */
public class FloatField extends PrimitiveOrWrapperField<Float> {

  /** {@link TypeMirror} for the primitive {@code float}. */
  public static final TypeMirror MIRROR =
      Environment.getTypeUtils().getPrimitiveType(TypeKind.FLOAT);

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param lowerBound lowerBound the lower bound.
   * @param upperBound upperBound the upper bound.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @throws IllegalArgumentException if {@code name} is {@code null}.
   */
  public FloatField(
      @NotNull final CharSequence name,
      final float lowerBound,
      final float upperBound,
      final boolean hasDefaultValue,
      final float defaultValue)
      throws IllegalArgumentException {
    super(FloatField.MIRROR, name, lowerBound, upperBound, hasDefaultValue, defaultValue);
  }

  @Override
  public boolean hasLowerBound() {
    return this.lowerBound() != Float.NEGATIVE_INFINITY;
  }

  @Override
  public boolean hasUpperBound() {
    return this.upperBound() != Float.POSITIVE_INFINITY;
  }
}
