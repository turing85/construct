package com.turing.construct.impl.model.fields.primitives;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@code byte}. */
public class ByteField extends PrimitiveOrWrapperField<Byte> {

  /** {@link TypeMirror} for the primitive {@code byte}. */
  public static final TypeMirror MIRROR =
      Environment.getTypeUtils().getPrimitiveType(TypeKind.BYTE);

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param lowerBound lowerBound the lower bound.
   * @param upperBound upperBound the upper bound.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @throws IllegalArgumentException if {@code name} is {@code null}.
   */
  public ByteField(
      @NotNull final CharSequence name,
      final byte lowerBound,
      final byte upperBound,
      final boolean hasDefaultValue,
      final byte defaultValue)
      throws IllegalArgumentException {
    super(ByteField.MIRROR, name, lowerBound, upperBound, hasDefaultValue, defaultValue);
  }

  @Override
  public boolean hasLowerBound() {
    return this.lowerBound() != Byte.MIN_VALUE;
  }

  @Override
  public boolean hasUpperBound() {
    return this.upperBound() != Byte.MAX_VALUE;
  }
}
