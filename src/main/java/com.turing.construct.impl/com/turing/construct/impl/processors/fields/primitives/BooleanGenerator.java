package com.turing.construct.impl.processors.fields.primitives;

import com.turing.construct.impl.annotations.primitives.BooleanBuildProperty;
import com.turing.construct.impl.model.fields.primitives.BooleanField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.lang.model.element.VariableElement;

/**
 * Generator for fields annotated with {@link BooleanBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link BooleanField}s.
 */
public class BooleanGenerator extends FieldGenerator {

  /** Constructor. */
  public BooleanGenerator() {
    super(BooleanBuildProperty.class, BooleanField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected Collection<Field> model(final List<VariableElement> annotated) {
    List<Field> booleanFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      BooleanBuildProperty annotation = field.getAnnotation(BooleanBuildProperty.class);
      booleanFields.add(
          new BooleanField(
              field.getSimpleName(), annotation.hasDefaultValue(), annotation.defaultValue()));
    }

    return booleanFields;
  }
}
