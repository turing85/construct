package com.turing.construct.impl.processors.fields.wrappers;

import com.turing.construct.impl.annotations.wrappers.IntegerWrapperBuildProperty;
import com.turing.construct.impl.model.fields.wrappers.IntegerWrapperField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;
import javax.validation.constraints.NotNull;

/**
 * Generator for fields annotated with {@link IntegerWrapperBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link IntegerWrapperField}s.
 */
public class IntegerWrapperGenerator extends FieldGenerator {

  /** Constructor. */
  public IntegerWrapperGenerator() {
    super(
        IntegerWrapperBuildProperty.class,
        IntegerWrapperField.MIRROR,
        FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> integerWrapperFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      IntegerWrapperBuildProperty annotation =
          field.getAnnotation(IntegerWrapperBuildProperty.class);
      boolean isNullable = (field.getAnnotation(NotNull.class) == null);
      integerWrapperFields.add(
          new IntegerWrapperField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue(),
              isNullable));
    }
    return integerWrapperFields;
  }
}
