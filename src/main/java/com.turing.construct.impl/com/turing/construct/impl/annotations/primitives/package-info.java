/**
 * Annotations for primitives.
 *
 * <p>Due to the nature of built-ins (having a default-value, being values, not references,...),
 * this package provides a separate annotation for each of the eight primitives.
 *
 * <p><b>Implementation notice:</b> In the current implementation, the wrappers of primitives can be
 * annotated with an annotation for primitives. This if for the case that your POJO needs something
 * like bindings, but you only want to expose primitives to the outside. This is realized by
 * allowing the {@code <Primitive>BuildProperty} on everything, that is in assignable form to the
 * primitive. This also means that one could annotate, e.g. an {@code Object} with e.g. {@code
 * IntBuildProperty}. The use of such techniques is highly discouraged.
 */
package com.turing.construct.impl.annotations.primitives;
