package com.turing.construct.impl.processors.fields.wrappers;

import com.turing.construct.impl.annotations.wrappers.LongWrapperBuildProperty;
import com.turing.construct.impl.model.fields.wrappers.LongWrapperField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;
import javax.validation.constraints.NotNull;

/**
 * Generator for fields annotated with {@link LongWrapperBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link LongWrapperField}s.
 */
public class LongWrapperGenerator extends FieldGenerator {

  /** Constructor. */
  public LongWrapperGenerator() {
    super(LongWrapperBuildProperty.class, LongWrapperField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> longWrapperFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      LongWrapperBuildProperty annotation = field.getAnnotation(LongWrapperBuildProperty.class);
      boolean isNullable = (field.getAnnotation(NotNull.class) == null);
      longWrapperFields.add(
          new LongWrapperField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue(),
              isNullable));
    }

    return longWrapperFields;
  }
}
