/**
 * Collection of wrapper field representations.
 *
 * <p>Each class only implements the abstract method {@link
 * com.turing.construct.impl.model.fields.PrimitiveOrWrapperField#hasLowerBound()} and {@link
 * com.turing.construct.impl.model.fields.PrimitiveOrWrapperField#hasUpperBound()}.
 *
 * <p>A lower (upper) bound exists, if it is not equal to the minimal (maximal) value of this type.
 * Due to this definition, {@link
 * com.turing.construct.impl.model.fields.wrappers.BooleanWrapperField} does not need an upper or
 * lower bound. Thus, the calls to {@link
 * com.turing.construct.impl.model.fields.primitives.BooleanField#hasLowerBound()} and {@link
 * com.turing.construct.impl.model.fields.wrappers.BooleanWrapperField#hasLowerBound()} will always
 * return {@code false}.
 */
package com.turing.construct.impl.model.fields.wrappers;
