package com.turing.construct.impl.processors.fields.wrappers;

import com.turing.construct.impl.annotations.wrappers.BooleanWrapperBuildProperty;
import com.turing.construct.impl.model.fields.wrappers.BooleanWrapperField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;
import javax.validation.constraints.NotNull;

/**
 * Generator for fields annotated with {@link BooleanWrapperBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link BooleanWrapperField}s.
 */
public class BooleanWrapperGenerator extends FieldGenerator {

  /** Constructor. */
  public BooleanWrapperGenerator() {
    super(
        BooleanWrapperBuildProperty.class,
        BooleanWrapperField.MIRROR,
        FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> booleanWrapperFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      BooleanWrapperBuildProperty annotation =
          field.getAnnotation(BooleanWrapperBuildProperty.class);
      boolean isNullable = (field.getAnnotation(NotNull.class) == null);
      booleanWrapperFields.add(
          new BooleanWrapperField(
              field.getSimpleName(),
              annotation.hasDefaultValue(),
              annotation.defaultValue(),
              isNullable));
    }
    return booleanWrapperFields;
  }
}
