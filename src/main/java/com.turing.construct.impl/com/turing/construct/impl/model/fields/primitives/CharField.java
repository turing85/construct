package com.turing.construct.impl.model.fields.primitives;

import com.turing.construct.Environment;
import com.turing.construct.impl.model.fields.PrimitiveOrWrapperField;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.validation.constraints.NotNull;

/** Representation of a field in assignable form to {@code char}. */
public class CharField extends PrimitiveOrWrapperField<Character> {

  /** {@link TypeMirror} for the primitive {@code char}. */
  public static final TypeMirror MIRROR =
      Environment.getTypeUtils().getPrimitiveType(TypeKind.CHAR);

  /**
   * Constructor.
   *
   * @param name name the name for the field.
   * @param lowerBound lowerBound the lower bound.
   * @param upperBound upperBound the upper bound.
   * @param hasDefaultValue defines whether this field has a default value.
   * @param defaultValue the default value for this field.
   * @throws IllegalArgumentException if {@code name} is {@code null}.
   */
  public CharField(
      @NotNull final CharSequence name,
      final char lowerBound,
      final char upperBound,
      final boolean hasDefaultValue,
      final char defaultValue)
      throws IllegalArgumentException {
    super(CharField.MIRROR, name, lowerBound, upperBound, hasDefaultValue, defaultValue);
  }

  @Override
  public boolean hasLowerBound() {
    return this.lowerBound() != Character.MIN_VALUE;
  }

  @Override
  public boolean hasUpperBound() {
    return this.upperBound() != Character.MAX_VALUE;
  }
}
