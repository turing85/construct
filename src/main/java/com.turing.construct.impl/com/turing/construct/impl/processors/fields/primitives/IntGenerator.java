package com.turing.construct.impl.processors.fields.primitives;

import com.turing.construct.impl.annotations.primitives.IntBuildProperty;
import com.turing.construct.impl.model.fields.primitives.IntField;
import com.turing.construct.model.Field;
import com.turing.construct.processors.FieldGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.VariableElement;

/**
 * Generator for fields annotated with {@link IntBuildProperty}.
 *
 * <p>The generated fields are represented in form of {@link IntField}s.
 */
public class IntGenerator extends FieldGenerator {

  /** Constructor. */
  public IntGenerator() {
    super(IntBuildProperty.class, IntField.MIRROR, FieldGenerator.DEFAULT_PRIORITY);
  }

  @Override
  protected List<Field> model(final List<VariableElement> annotated) {
    List<Field> intFields = new ArrayList<>();

    for (VariableElement field : annotated) {
      IntBuildProperty annotation = field.getAnnotation(IntBuildProperty.class);
      intFields.add(
          new IntField(
              field.getSimpleName(),
              annotation.lowerBound(),
              annotation.upperBound(),
              annotation.hasDefaultValue(),
              annotation.defaultValue()));
    }

    return intFields;
  }
}
