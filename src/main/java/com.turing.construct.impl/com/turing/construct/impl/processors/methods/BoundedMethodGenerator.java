package com.turing.construct.impl.processors.methods;

import com.turing.construct.Environment;
import com.turing.construct.ProcessorException;
import com.turing.construct.impl.model.BoundedMethod;
import com.turing.construct.model.BoundedField;
import com.turing.construct.model.Clazz;
import com.turing.construct.model.Field;
import com.turing.construct.model.Method;
import com.turing.construct.processors.MethodGenerator;

public class BoundedMethodGenerator extends MethodGenerator {

  public BoundedMethodGenerator() {
    super(BoundedField.class, MethodGenerator.DEFAULT_PRIORITY);
  }

  @Override
  public boolean accepts(Field field) {
    return BoundedField.class.isAssignableFrom(field.getClass());
  }

  @SuppressWarnings("unchecked")
  @Override
  public Method constructMethod(final Clazz inClass, final Field forField)
      throws ProcessorException {
    if (this.accepts(forField) == false) {
      Environment.getMessager()
          .error("Field %s is no instance of %s", forField, this.generateFor().getSimpleName());
      throw new ProcessorException();
    }

    if (forField instanceof BoundedField<?>) {
      return new BoundedMethod<>(inClass, ((BoundedField<?>) forField));
    }

    throw new AssertionError();
  }
}
