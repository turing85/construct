module com.turing.construct.impl {
  requires transitive com.turing.construct;
  requires java.compiler;
  requires java.validation;

  exports com.turing.construct.impl.annotations.primitives;
  exports com.turing.construct.impl.annotations.wrappers;
  exports com.turing.construct.impl.model.fields;
  exports com.turing.construct.impl.model.fields.primitives;
  exports com.turing.construct.impl.model.fields.wrappers;
  exports com.turing.construct.impl.processors.fields.primitives;
  exports com.turing.construct.impl.processors.fields.wrappers;

  provides com.turing.construct.processors.FieldGenerator with
      com.turing.construct.impl.processors.fields.primitives.BooleanGenerator,
      com.turing.construct.impl.processors.fields.primitives.ByteGenerator,
      com.turing.construct.impl.processors.fields.primitives.ShortGenerator,
      com.turing.construct.impl.processors.fields.primitives.IntGenerator,
      com.turing.construct.impl.processors.fields.primitives.LongGenerator,
      com.turing.construct.impl.processors.fields.primitives.FloatGenerator,
      com.turing.construct.impl.processors.fields.primitives.DoubleGenerator,
      com.turing.construct.impl.processors.fields.primitives.CharGenerator,
      com.turing.construct.impl.processors.fields.wrappers.BooleanWrapperGenerator,
      com.turing.construct.impl.processors.fields.wrappers.ByteWrapperGenerator,
      com.turing.construct.impl.processors.fields.wrappers.ShortWrapperGenerator,
      com.turing.construct.impl.processors.fields.wrappers.IntegerWrapperGenerator,
      com.turing.construct.impl.processors.fields.wrappers.LongWrapperGenerator,
      com.turing.construct.impl.processors.fields.wrappers.FloatWrapperGenerator,
      com.turing.construct.impl.processors.fields.wrappers.DoubleWrapperGenerator,
      com.turing.construct.impl.processors.fields.wrappers.CharacterWrapperGenerator;
  provides com.turing.construct.processors.MethodGenerator with
      com.turing.construct.impl.processors.methods.BoundedMethodGenerator;
}
